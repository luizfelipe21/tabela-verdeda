import re
import PySimpleGUI as sg

class Passa(object):
    pass

class Tabela(object):
    def __init__(self, variaveis = None, formulas = None):
        self.variaveis = variaveis     #recebe as variaveis
        self.formulas = formulas      #recebe as formulas
        self.tabela = []    #cria a tabela
        largura = len(variaveis)       #largura = n de variaveis
        for i in range(0, 2 ** (largura)):           # criando as linhas de 0 a 7
            a = bin(i)[2:].zfill(largura)          # calcula o valor binario de i, eliminando 0b e preenchendo todas as casas
            lista = []
            for j in range(len(a)):         # cria as colunas
                lista.append(int(a[j]))         # guarda na lista o valor 0 ou 1
            self.tabela.append(lista)         # adiciona a linha criada a tabela
        self.r = re.compile(r'(?<!\w)(' + '|'.join(self.variaveis) + ')(?!\w)')         # expressao a ser usada varias vezes

    def calcula(self, *args):
        p = Passa()         # somente para preencher uma lacuna
        for i, j in zip(self.variaveis, args):
            setattr(p, i, j)         # atribuir ao obj p de nome i um valor j
        formulas = []
        for i in self.formulas:         # para resolver cada uma das formulas passadas
            i = self.r.sub(r'p.\1', i)         # calcula o resultado a partir de i
            formulas.append(eval(i))         # adiciona o resultado a lista 'formulas'
        linha = [getattr(p, j) for j in self.variaveis] + formulas         # retorna o valor de nome j dentro do obj p + resultado
        return linha         # retorna linha com os valores das variaveis e o resultado da linha

    def imprime(self):
        lista = []
        for i in self.tabela:
            lista.append(self.calcula(*i))         #chamando a funcao e guardando a linha retornada dentro da lista
        print('TABELA VERDADE')
        for i in range(len(self.variaveis)):
            print(self.variaveis[i], end = ' | ')         #imprime as variaveis
        for i in range(len(self.formulas)):
            print(self.formulas[i], end='')             #imprime as formulas
        print('')
        c = 0
        for i in range(len(lista)):
            for j in range(len(lista[i])):
                if j != len(lista[i])-1:
                    print(lista[i][j], end = ' | ')           # imprime os valores de posicao i, j da lista
                else:
                    if int(lista[i][j]) == 1:
                        c += 1
                    print(int(lista[i][j]))
        if c == 2 ** len(self.variaveis):           #se todos os resultados finais forem = 1 e tautologia
            print('Forma tautologia.')
        else:
            print('Nao forma tautologia.')

sg.theme('DarkGrey')
layout = [
    [sg.Text('*AND: x and y               *NAND: NOT AND')],
    [sg.Text('*OR: x or y                   *NOR: NOT OR')],
    [sg.Text('*Equivalente: x == y      *NOT: not x')],
    [sg.Text('*XOR: x != y                 *Implicacao: (not x) or y')],
    [sg.Text('_______________________________________')],
    [sg.Text('Separe as variaveis por virgulas.')],
    [sg.Text('Variaveis:  '),sg.Input(key = 'variaveis', size=(27,1))],
    [sg.Text('Formulas:  '),sg.Input(key = 'formulas', size=(27,1))],
    [sg.Button(' Ok ')],
    [sg.Output(size=(37,15))]]
janela = sg.Window('Tabela verdade', layout= layout)

while True:
    eventos, valores = janela.read()
    variaveis = valores['variaveis'].strip().lower().split(',')
    formulas = valores['formulas'].strip().lower().split(',')
    try:
        a = Tabela(variaveis,formulas)
        a.imprime()
    except:
        print('Erro na digitacao.')

